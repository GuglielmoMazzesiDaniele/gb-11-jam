using UnityEngine;

/// <summary>
/// Class that contains a GameObject and all the relative data necessary to spawn it.
/// </summary>
[CreateAssetMenu(menuName = "Spawn/" + nameof(SpawnModel), fileName = nameof(SpawnModel))]
public class SpawnModel : ScriptableObject
{
    public GameObject ObjectToSpawn;
    
    /// <summary>
    /// This method spawns the given object into world space
    /// </summary>
    public void Spawn(Transform spawnPoint)
    {
        Instantiate(ObjectToSpawn, spawnPoint.position, spawnPoint.rotation);
    }
}
