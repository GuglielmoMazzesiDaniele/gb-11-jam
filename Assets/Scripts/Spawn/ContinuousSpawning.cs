using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class ContinuousSpawning : MonoBehaviour
{
    [Tooltip("The spawn point from which the model will be spawned")]
    public Transform SpawnPoint;
    [Tooltip("The model that gets spawned")]
    public SpawnModel Model;
    [Tooltip("The UnityEvent that get launched whenever the model gets spawned")]
    public UnityEvent Spawn;
    [Tooltip("The max spawn frequency, expressed in min seconds between spawns")]
    public float Frequency;

    private Coroutine m_spawning;

    public void OnEnable()
    {
        m_spawning = StartCoroutine(DoSpawn());
    }

    public void OnDisable()
    {
        StopCoroutine(m_spawning);
    }

    private IEnumerator DoSpawn()
    {
        while (true)
        {
            Model.Spawn(SpawnPoint);
            Spawn.Invoke();
            yield return new WaitForSeconds(Frequency);
        }
    }
}
