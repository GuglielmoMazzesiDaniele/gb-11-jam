using UnityEngine;
using UnityEngine.Events;

public class GBInputBasedSpawning : MonoBehaviour
{
    [Tooltip("The spawn point from which the model will be spawned")]
    public Transform SpawnPoint;
    [Tooltip("Hooks used to decided when to spawn")]
    public GBHooks Hooks;
    [Tooltip("The model that gets spawned")]
    public SpawnModel Model;
    [Tooltip("The UnityEvent that get launched whenever the model gets spawned")]
    public UnityEvent Spawn;
    [Tooltip("The max spawn frequency, expressed in min seconds between spawns")]
    public float Frequency;

    private float lastSpawn;

    public void Start()
    {
        Hooks.A.AddListener(DoSpawn);
    }

    private void DoSpawn()
    {
        if (Time.time - lastSpawn >= Frequency)
        {
            Model.Spawn(SpawnPoint);
            Spawn.Invoke();
            lastSpawn = Time.time;
        }
    }
}
