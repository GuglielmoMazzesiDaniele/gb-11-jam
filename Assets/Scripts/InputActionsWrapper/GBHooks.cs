using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

/// <summary>
/// A SO containing all the input actions possible on a Game Boy
/// </summary>
[CreateAssetMenu(menuName = "GB/Utility/InputActions", fileName = nameof(GBHooks))]
public class GBHooks : ScriptableObject
{
    [SerializeField] [Tooltip("The backing Input Action Asset of this component")]
    private InputActionAsset m_actionAsset;

    [NonSerialized] public UnityEvent<Vector2> DPad;

    [NonSerialized] public UnityEvent A;

    [NonSerialized] public UnityEvent B;

    [NonSerialized] public UnityEvent Select;

    [NonSerialized] public UnityEvent Start;

    public void OnEnable()
    {
        // Events initialization
        DPad = new UnityEvent<Vector2>();
        A = new UnityEvent();
        B = new UnityEvent();
        Select = new UnityEvent();
        Start = new UnityEvent();
        // Enabling the InputActionAsset
        this.m_actionAsset.Enable();
        // Connecting every backing action with their corresponding Unity Event
        this.m_actionAsset.FindAction("DPad").performed += 
            context => DPad.Invoke(context.ReadValue<Vector2>());
        this.m_actionAsset.FindAction("DPad").canceled +=
            context => DPad.Invoke(new Vector2(0, 0));
        this.m_actionAsset.FindAction("A").started +=
            context => A.Invoke();
        this.m_actionAsset.FindAction("B").started +=
            context => B.Invoke();
        this.m_actionAsset.FindAction("Select").started +=
            context => Select.Invoke();
        this.m_actionAsset.FindAction("Start").started +=
            context => Start.Invoke();
    }

    public void OnDisable()
    {
        this.m_actionAsset.Disable();
    }
}