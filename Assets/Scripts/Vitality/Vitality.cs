using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Class implementing a Vitality system
/// </summary>
public class Vitality : MonoBehaviour, IDamageable
{
    public int MaxHealth;
    public UnityEvent Death;

    private int m_current;
    
    public void Start()
    {
        m_current = MaxHealth;
        Death = new UnityEvent();
    }

    public void Damage(int damage)
    {
        this.m_current = Mathf.Max(0, m_current - damage);
        if (m_current == 0)
        {
            Death.Invoke();
            Destroy(gameObject);
        }
    }

    public void Heal(int healing)
    {
        this.m_current = Mathf.Max(MaxHealth, m_current + healing);
    }
}