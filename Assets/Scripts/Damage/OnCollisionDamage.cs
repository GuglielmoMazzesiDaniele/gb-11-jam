using System;
using UnityEngine;

/// <summary>
/// Class that applies a damage amount on collision
/// </summary>
public class OnCollisionDamage : MonoBehaviour
{
    [Tooltip("The damage this GameObject does on collision")]
    public int Damage;

    [Tooltip("Weather or not this GameObject gets destroyed on collision")]
    public bool DestroyOnCollision;

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.TryGetComponent(out IDamageable target))
        {
            target.Damage(Damage);
        }
        if(DestroyOnCollision)
            Destroy(gameObject);
    }
}
