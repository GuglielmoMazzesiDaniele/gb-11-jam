using UnityEngine;

public interface IAI
{
    public void StartDefending(Vector3 direction);
    public void InvertDirection();
}
