public interface IDamageable
{
    public void Damage(int damage);
    public void Heal(int healing);
}
