using UnityEngine;
using Random = System.Random;

public class DefendingLine : MonoBehaviour
{
    private Random random;

    public void Start()
    {
        random = new Random();
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.TryGetComponent(out IAI AI))
        {
            int coinFlip = random.Next(99);
            if (coinFlip < 50)
            {
                AI.StartDefending(new Vector3(1, 0 ,0 ));
            }
            else
            {
                AI.StartDefending(new Vector3(-1, 0, 0));
            }
        }
    }

    public void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.TryGetComponent(out IAI AI))
        {
            AI.InvertDirection();
        }
    }
}
