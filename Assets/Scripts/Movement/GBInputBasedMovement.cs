using System.Collections;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] [Tooltip("The Hooks used by this component")]
    private GBHooks m_inputs;
    
    [Tooltip("The current movement speed of the player")]
    public float Speed;

    // The moving coroutine
    private Coroutine movementCoroutine;
    
    // The target transform
    private Transform target;

    /// <summary>
    /// Caching the required components
    /// </summary>
    public void Start()
    {
        this.target = this.gameObject.transform;
    }
    
    /// <summary>
    /// 
    /// Subscribing to Hooks
    /// </summary>
    public void OnEnable()
    {
        this.m_inputs.DPad.AddListener(Move);
    }

    /// <summary>
    /// Implementation of movement for the player
    /// </summary>
    /// <param name="normalizedDirection">The normalized direction of the movement</param>
    public void Move(Vector2 normalizedDirection)
    {
        // If the coroutine is not running then stop it
        if (movementCoroutine == null)
            movementCoroutine = StartCoroutine(DoMove(normalizedDirection));
        else
        {
            StopCoroutine(movementCoroutine);
            movementCoroutine = StartCoroutine(DoMove(normalizedDirection));
        }
    }

    /// <summary>
    /// Coroutine in charge of moving the target transform based on the provided Vector2
    /// </summary>
    /// <param name="normalizedDirection">Vector2 representing the normalized movement direction</param>
    /// <returns></returns>
    private IEnumerator DoMove(Vector2 normalizedDirection)
    {
        while (true)
        {
            // Calculating Vector3 representing position of the GameObject in space
            Vector3 normalizedDirection3D =
                new Vector3(normalizedDirection.x * Speed, normalizedDirection.y * Speed,0);
            // Caching the position for calculation
            var position = transform.position;
            // Applying a smooth transition
            transform.position =
                Vector3.Lerp(position,
                    position + normalizedDirection3D,
                    Time.deltaTime);
            yield return null;
        }
    }
}
