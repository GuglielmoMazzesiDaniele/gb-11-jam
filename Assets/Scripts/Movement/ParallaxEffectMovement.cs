using UnityEngine;

public class ParallaxEffectMovement : MonoBehaviour
{
    private Transform m_transform;
    private Transform m_cameraTransform;
    private float m_textureWidthInUnits;
    private float m_textureHeightInUnits;
    
    public float Speed;
    public Vector3 Direction;

    public void Start()
    {
        // Caching the camera transform
        m_cameraTransform = Camera.main.transform;
        // Caching this object transform
        m_transform = transform;
        // Obtaining the rendered texture size in Unity's units
        Sprite attachedSprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D renderedTexture = attachedSprite.texture;
        m_textureHeightInUnits = renderedTexture.height / attachedSprite.pixelsPerUnit;
        m_textureWidthInUnits = renderedTexture.width / attachedSprite.pixelsPerUnit;
    }

    public void LateUpdate()
    {
        transform.position += Direction * (Speed * Time.deltaTime);
        // Checking if the sprite is about to exit the camera FOV on the Y axis. If it is then move it
        if (Mathf.Abs(m_cameraTransform.position.y - m_transform.position.y) >= m_textureHeightInUnits)
        {
            // Offset to avoid a visible clipping when moving the texture
            float offsetY = (m_cameraTransform.position.y - m_transform.position.y) % m_textureHeightInUnits;
            m_transform.position =
                new Vector3(m_transform.position.x, m_cameraTransform.position.y + offsetY, m_transform.position.z);
        }
        // Checking if the sprite is about to exit the camera FOV on the X axis. If it is then move it
        if (Mathf.Abs(m_cameraTransform.position.x - m_transform.position.x) >= m_textureWidthInUnits)
        {
            m_transform.position =
                new Vector3(m_cameraTransform.position.x, m_transform.position.y, m_transform.position.z);
        }
    }
}
