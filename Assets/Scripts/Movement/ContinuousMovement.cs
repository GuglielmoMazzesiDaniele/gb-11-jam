using UnityEngine;

public class ContinuousMovement : MonoBehaviour
{
    [Tooltip("The direction which the projectile will go.")]
    public Vector3 Direction;
    [Tooltip("The speed at which the projectile will move.")]
    public float Speed;

    // Cached transform reference
    private Transform m_transform;

    public void Start()
    {
        m_transform = transform;
    }

    public void Update()
    {
        m_transform.position += Direction * (Speed * Time.deltaTime);
    }
}
