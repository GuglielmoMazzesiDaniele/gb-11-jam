using UnityEngine;

public class EnemyMovement : MonoBehaviour, IAI
{
    private enum AIState
    {
        IDLE,
        DEFENDING
    }
    
    [Tooltip("The movement speed of this enemy")]
    public float Speed;
    [Tooltip("The direction the AI is moving")]
    public Vector3 Direction;
    
    private Transform m_tranform;
    private AIState m_state;

    public void Start()
    {
        m_tranform = transform;
    }

    public void Update()
    {
        this.transform.position += Direction * (Speed * Time.deltaTime);
    }

    public void StartDefending(Vector3 direction)
    {
        if (m_state != AIState.DEFENDING)
        {
            m_state = AIState.DEFENDING;
            this.Direction = direction;
        }
    }

    public void InvertDirection()
    {
        Direction.x = -Direction.x;
    }
}
